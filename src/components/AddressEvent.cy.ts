import AddressEvent from "./AddressEvent.vue";

describe("<AddressEvent />", () => {
  describe("deposit action", () => {
    beforeEach(() => {
      cy.mount(AddressEvent, {
        props: {
          address: "0x0ab",
          eventType: "deposit",
          asset: "ETH",
          amount: "5",
          usd: "500 $",
          date: "1 January 2020",
        },
      });
    });
    it("renders", () => {
      cy.get("[component-test-address-event]").should("be.visible");
    });
    it("renders address correctly", () => {
      cy.get("[component-test-address]")
        .should("be.visible")
        .and("contain.text", "Address: 0x0ab");
    });
    it("renders message correctly", () => {
      cy.get("[component-test-message]")
        .should("be.visible")
        .and("contain.text", "Deposited 5 ETH for a value of 500 $");
    });
    it("renders date correctly", () => {
      cy.get("[component-test-date]")
        .should("be.visible")
        .and("contain.text", "On date: 1 January 2020");
    });
  });

  describe("withdraw action", () => {
    beforeEach(() => {
      cy.mount(AddressEvent, {
        props: {
          address: "0x1cd",
          eventType: "withdraw",
          asset: "DAI",
          amount: "15",
          usd: "42 $",
          date: "1 January 2022",
        },
      });
    });
    it("renders", () => {
      cy.get("[component-test-address-event]").should("be.visible");
    });
    it("renders address correctly", () => {
      cy.get("[component-test-address]")
        .should("be.visible")
        .and("contain.text", "Address: 0x1cd");
    });
    it("renders message correctly", () => {
      cy.get("[component-test-message]")
        .should("be.visible")
        .and("contain.text", "Withdrawn 15 DAI for a value of 42 $");
    });
    it("renders date correctly", () => {
      cy.get("[component-test-date]")
        .should("be.visible")
        .and("contain.text", "On date: 1 January 2022");
    });
  });
});
