import AssetToken from "./AssetToken.vue";
import type { SupportedAssets } from "../types";

describe("<AssetToken />", () => {
  describe("asset isn't a valid asset", () => {
    beforeEach(() => {
      cy.mount(AssetToken, {
        props: {
          asset: "DOGE" as SupportedAssets,
        },
      });
    });

    it("renders", () => {
      cy.get("[test-component-asset-token]").should("be.visible");
    });
    it("shows the placeholder image", () => {
      cy.get("[test-component-asset-icon]").should("be.visible");
      cy.get("[test-component-asset-icon]")
        .invoke("attr", "src")
        .then((src) => expect(src?.match("gravatar.com")));
      cy.get("[test-component-asset-icon]")
        .invoke("attr", "alt")
        .should("eq", "DOGE");
    });
    it("shows the label", () => {
      cy.get("[test-component-asset-token-label]")
        .should("be.visible")
        .and("contain.text", "DOGE");
    });
  });

  describe("asset is a valid asset", () => {
    beforeEach(() => {
      cy.mount(AssetToken, {
        props: {
          asset: "ETH",
        },
      });
    });

    it("renders", () => {
      cy.get("[test-component-asset-token]").should("be.visible");
    });
    it("shows the token image", () => {
      cy.get("[test-component-asset-icon]").should("be.visible");
      cy.get("[test-component-asset-icon]")
        .invoke("attr", "src")
        .then((src) => expect(src?.match("1inch.io")));
      cy.get("[test-component-asset-icon]")
        .invoke("attr", "alt")
        .should("eq", "ETH");
    });
    it("shows the label", () => {
      cy.get("[test-component-asset-token-label]")
        .should("be.visible")
        .and("contain.text", "ETH");
    });
  });
});
