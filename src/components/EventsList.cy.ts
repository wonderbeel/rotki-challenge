import EventsList from "./EventsList.vue";

describe("<EventsList />", () => {
  describe("display the list correctly", () => {
    beforeEach(() => {
      cy.mount(EventsList, {
        props: {
          list: [
            {
              address: "0x01ab",
              events: [
                {
                  event_type: "deposit",
                  asset: "ETH",
                  value: {
                    amount: "5",
                    usd_value: "500",
                  },
                  timestamp: 1570944237,
                },
                {
                  event_type: "withdraw",
                  asset: "GRT",
                  value: {
                    amount: "500",
                    usd_value: "15",
                  },
                  timestamp: 1571172272,
                },
              ],
            },
          ],
        },
      });
    });

    it("renders", () => {
      cy.get("[test-component-events-list-wrapper]").should("be.visible");
      cy.get("[test-component-events-list-filter]").should("be.visible");
      cy.get("[test-component-events-list]").should("be.visible");
    });
    it("is showing all items", () => {
      cy.get("[test-component-events-list-active-filter]")
        .should("be.visible")
        .and("contain.text", "Showing events for: All addresses");
    });
    it("renders childrens correctly", () => {
      cy.get("[component-test-address-event]")
        .should("be.visible")
        .and("have.length", 2);
    });
    it("formats dates correctly", () => {
      cy.get("[component-test-address-event]")
        .get("[component-test-date]")
        .first()
        .should("contain.text", "On date: 13 October 2019");
    });
  });

  describe("can filter", () => {
    beforeEach(() => {
      cy.mount(EventsList, {
        props: {
          list: [
            {
              address: "0x01ab",
              events: [
                {
                  event_type: "deposit",
                  asset: "ETH",
                  value: {
                    amount: "5",
                    usd_value: "500",
                  },
                  timestamp: 1570944237,
                },
                {
                  event_type: "withdraw",
                  asset: "GRT",
                  value: {
                    amount: "500",
                    usd_value: "15",
                  },
                  timestamp: 1571172272,
                },
              ],
            },
            {
              address: "0x02cd",
              events: [
                {
                  event_type: "deposit",
                  asset: "DAI",
                  value: {
                    amount: "3",
                    usd_value: "70",
                  },
                  timestamp: 1571578878,
                },
              ],
            },
          ],
        },
      });
    });

    it("renders", () => {
      cy.get("[test-component-events-list-wrapper]").should("be.visible");
      cy.get("[test-component-events-list-filter]").should("be.visible");
      cy.get("[test-component-events-list]").should("be.visible");
    });
    it("is showing all items", () => {
      cy.get("[test-component-events-list-active-filter]")
        .should("be.visible")
        .and("contain.text", "Showing events for: All addresses");
    });
    it("renders childrens correctly", () => {
      cy.get("[component-test-address-event]")
        .should("be.visible")
        .and("have.length", 3);
    });
    it("filters correctly", () => {
      cy.get("[test-component-events-list-select]").select("0x02cd");
      cy.get("[test-component-events-list-active-filter]")
        .should("be.visible")
        .and("contain.text", "Showing events for: 0x02cd");
      cy.get("[component-test-address-event]")
        .should("be.visible")
        .and("have.length", 1);
    });
  });
});
