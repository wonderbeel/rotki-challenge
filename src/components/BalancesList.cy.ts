import BalancesList from "./BalancesList.vue";

describe("<BalancesList />", () => {
  beforeEach(() => {
    cy.mount(BalancesList, {
      props: {
        totalBalance: 500,
        list: [
          {
            asset: "ETH",
            amount: 5,
            usd_value: 50,
          },
          {
            asset: "DAI",
            amount: 50,
            usd_value: 450,
          },
        ],
      },
    });
  });
  it("renders", () => {
    cy.get("[test-component-balances-list-wrapper]").should("be.visible");
    cy.get("[test-component-balances-list]").should("be.visible");
  });
  it("shows totalBalance", () => {
    cy.get("[test-component-balances-list-total-balance]")
      .should("be.visible")
      .and("contain.text", "500")
      .and("contain.text", "$");
  });
  it("renders the list of childrens correctly", () => {
    cy.get("[test-component-asset-balance-item]").should("have.length", 2);
  });
});
