import AssetBalance from "./AssetBalance.vue";

describe("<AssetBalance />", () => {
  beforeEach(() => {
    cy.mount(AssetBalance, {
      props: {
        asset: "ETH",
        amount: "10",
        usd: "50 $",
        percentage: "10",
      },
    });
  });
  it("renders", () => {
    cy.get("[test-component-asset-balance-item]").should("be.visible");
  });
  it("renders amount correctly", () => {
    cy.get("[test-component-asset-balance-amount]")
      .should("be.visible")
      .and("contain.text", "Amount: 10");
  });
  it("renders usd correctly", () => {
    cy.get("[test-component-asset-balance-usd]")
      .should("be.visible")
      .and("contain.text", "USD Value: 50 $");
  });
  it("renders percentage correctly", () => {
    cy.get("[test-component-asset-balance-percentage]")
      .should("be.visible")
      .and("contain.text", "Percentage: 10%");
  });
});
