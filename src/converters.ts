import { unref } from "vue";
import { isValidAddress } from "./helpers";

import type { Ref } from "vue";
import type { Balances, AddressBalance, Events, AddressEvents } from "./types";

const queryToAddressBalanceArray = (data: Ref<Balances> | Ref<undefined>) => {
  const result = new Array<AddressBalance>();
  const loadedData = unref(data);
  if (loadedData) {
    Object.keys(loadedData).forEach((address: string) => {
      // check if address is actually present into the query result
      // and if it is actually a valid wallet address or not
      if (loadedData[address] && isValidAddress(address)) {
        const item = {
          address: address,
          balance: loadedData[address],
        };
        result.push(item);
      }
    });
  }
  return result;
};

const queryToAddressEventsArray = (data: Ref<Events> | Ref<undefined>) => {
  const result = new Array<AddressEvents>();
  const loadedData = unref(data);
  if (loadedData) {
    Object.keys(loadedData).forEach((address: string) => {
      if (loadedData[address] && isValidAddress(address)) {
        const item = {
          address: address,
          events: loadedData[address].events,
        };
        result.push(item);
      }
    });
  }
  return result;
};

export { queryToAddressBalanceArray, queryToAddressEventsArray };
