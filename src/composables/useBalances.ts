import { computed, ref } from "vue";
import { deepUnref } from "../helpers";

import { type ComputedRef, Ref } from "vue";
import type {
  SupportedAssets,
  AssetBalance,
  AssetInfo,
  AddressBalance,
} from "../types";

export const useBalances = (
  wallets:
    | ComputedRef<AddressBalance[]>
    | Ref<AddressBalance[]>
    | AddressBalance[]
) => {
  // use a map as an accumulator to calculate amount and usd_value of every asset
  const balancesMap = ref(new Map<SupportedAssets, AssetBalance>());
  // transform the map containing all the balances to an array
  const balances = computed<AssetInfo[]>(() => {
    const result = new Array<AssetInfo>();
    balancesMap.value.forEach(({ amount, usd_value }, key) => {
      result.push({
        asset: key,
        amount,
        usd_value,
      });
    });
    return result;
  });

  const totalUSDValue = computed(() =>
    balances.value.map(({ usd_value }) => usd_value).reduce((a, b) => a + b, 0)
  );

  // Compute the total balances
  const calcBalances = () => {
    // remove old values
    balancesMap.value.clear();
    const unreffedWallets = deepUnref(wallets);
    unreffedWallets.forEach(({ balance }) => {
      const assets = Object.keys(balance) as SupportedAssets[];
      assets.forEach((asset) => {
        // convert amount and usd_value to numbers
        const amount = Number(balance?.[asset]?.amount);
        const usd_value = Number(balance?.[asset]?.usd_value);
        // load the previous value or 0 if it is the first time that we found this asset
        const acc = balancesMap.value.get(asset) ?? { amount: 0, usd_value: 0 };
        balancesMap.value.set(asset, {
          amount: amount + acc.amount,
          usd_value: usd_value + acc.usd_value,
        });
      });
    });
  };

  return {
    balancesMap,
    balances,
    calcBalances,
    totalUSDValue,
  };
};
