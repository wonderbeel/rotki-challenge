import { describe, expect, test } from "vitest";
import { useBalances } from "./useBalances";

const setup = () => {
  const { balances, totalUSDValue, calcBalances } = useBalances([
    {
      address: "0xtest",
      balance: {
        ETH: {
          amount: "1",
          usd_value: "100",
        },
        DAI: {
          amount: "3",
          usd_value: "50",
        },
      },
    },
    {
      address: "0xtest2",
      balance: {
        ETH: {
          amount: "4",
          usd_value: "5000",
        },
        DAI: {
          amount: "2",
          usd_value: "300",
        },
        GRT: {
          amount: "10",
          usd_value: "10",
        },
      },
    },
  ]);
  return { balances, totalUSDValue, calcBalances };
};

describe("useBalances", () => {
  test("balances", () => {
    const { balances, calcBalances } = setup();
    calcBalances();
    expect(balances.value).toEqual([
      {
        asset: "ETH",
        amount: 5,
        usd_value: 5100,
      },
      {
        asset: "DAI",
        amount: 5,
        usd_value: 350,
      },
      {
        asset: "GRT",
        amount: 10,
        usd_value: 10,
      },
    ]);
  });

  test("totalUSDValue", () => {
    const { totalUSDValue, calcBalances } = setup();
    calcBalances();
    expect(totalUSDValue.value).toBe(5460);
  });
});
