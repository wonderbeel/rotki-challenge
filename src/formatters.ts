const locale = navigator.language ?? "en-US";

const numberFormatter = new Intl.NumberFormat(locale, {
  notation: "compact",
  compactDisplay: "short",
  minimumFractionDigits: 0,
  maximumFractionDigits: 3,
});

const usdFormatter = new Intl.NumberFormat(locale, {
  style: "currency",
  currency: "USD",
  compactDisplay: "short",
  currencyDisplay: "narrowSymbol",
  notation: "compact",
  minimumFractionDigits: 1,
  maximumFractionDigits: 2,
});

const addressFormatter = (address: string, digits = 4) =>
  `0x${address.slice(2, 2 + digits)}...${address.slice(
    address.length - digits
  )}`;

export { numberFormatter, usdFormatter, addressFormatter };
