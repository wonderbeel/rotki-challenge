import { describe, expect, test } from "vitest";
import { numberFormatter, usdFormatter, addressFormatter } from "./formatters";

describe("numberFormatter", () => {
  test("format number without decimals", () => {
    expect(numberFormatter.format(100)).toBe("100");
  });
  test("format number with decimals", () => {
    expect(numberFormatter.format(100.1)).toBe("100.1");
  });
  test("format number with long decimals", () => {
    expect(numberFormatter.format(100.123456)).toBe("100.123");
  });
});

describe("usdFormatter", () => {
  test("format 100", () => {
    expect(usdFormatter.format(100)).toBe("$100.0");
  });
  test("format 1000", () => {
    expect(usdFormatter.format(1000)).toBe("$1.0K");
  });
});

describe("addressFormatter", () => {
  test("format lower case address", () => {
    expect(addressFormatter("0xabc11a5acc3ad66025c21f24a91dd71d0fc28a46")).toBe(
      "0xabc1...8a46"
    );
  });
  test("format upper case address", () => {
    expect(addressFormatter("0xABC11A5ACC3AD66025C21F24A91DD71D0FC28A46")).toBe(
      "0xABC1...8A46"
    );
  });
  test("format mixed case address", () => {
    expect(addressFormatter("0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a46")).toBe(
      "0xABC1...8a46"
    );
  });
  test("format address with custom digits", () => {
    expect(
      addressFormatter("0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a46", 6)
    ).toBe("0xABC11a...c28a46");
  });
});
