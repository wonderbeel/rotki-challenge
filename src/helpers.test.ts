import { describe, expect, test } from "vitest";
import { computed, ref } from "vue";
import { isValidAddress, deepUnref, calcPercentage } from "./helpers";

describe("isValidAddress", () => {
  test("address is in lower case", () => {
    expect(isValidAddress("0xabc11a5acc3ad66025c21f24a91dd71d0fc28a46")).toBe(
      true
    );
  });
  test("address is in upper case", () => {
    expect(isValidAddress("0xABC11A5ACC3AD66025C21F24A91DD71D0FC28A46")).toBe(
      true
    );
  });
  test("address is in mixed case", () => {
    expect(isValidAddress("0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a46")).toBe(
      true
    );
  });
  test("address is too long", () => {
    expect(isValidAddress("0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a4666")).toBe(
      false
    );
  });
  test("address is too short", () => {
    expect(isValidAddress("0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a")).toBe(
      false
    );
  });
  test("address contains an invalid character", () => {
    expect(isValidAddress("0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28x46")).toBe(
      false
    );
  });
});

describe("deepUnref", () => {
  test("unref a string", () => {
    expect(deepUnref("hello world")).toBe("hello world");
  });
  test("unref a ref string", () => {
    expect(deepUnref(ref("hello world"))).toBe("hello world");
  });
  test("unref a computed string", () => {
    expect(deepUnref(computed(() => "hello world"))).toBe("hello world");
  });
});

describe("calcPercentage", () => {
  test("0% percentage", () => {
    expect(calcPercentage(0, 1000)).toBe(0);
  });
  test("1% percentage", () => {
    expect(calcPercentage(10, 1000)).toBe(1);
  });
  test("10% percentage", () => {
    expect(calcPercentage(100, 1000)).toBe(10);
  });
  test("50% percentage", () => {
    expect(calcPercentage(500, 1000)).toBe(50);
  });
  test("100% percentage", () => {
    expect(calcPercentage(1000, 1000)).toBe(100);
  });
});
