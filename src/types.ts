type SupportedAssets =
  | "ETH"
  | "DAI"
  | "UNI"
  | "AAVE"
  | "COMP"
  | "SUSHI"
  | "MKR"
  | "YFI"
  | "GRT"
  | "GTC";
type UserEvents = "withdraw" | "deposit";

type AssetAmount = {
  amount: string;
  usd_value: string;
};

type AssetBalance = {
  amount: number;
  usd_value: number;
};

interface AssetInfo extends AssetBalance {
  asset: SupportedAssets;
}

type WalletBalance = Partial<Record<SupportedAssets, AssetAmount>>;

type WalletEvent = {
  event_type: UserEvents;
  asset: SupportedAssets;
  value: AssetAmount;
  timestamp: number;
};

interface Balances {
  [wallet: string]: WalletBalance;
}

interface Events {
  [wallet: string]: {
    events: WalletEvent[];
  };
}

interface AddressBalance {
  address: string;
  balance: WalletBalance;
}

interface AddressEvents {
  address: string;
  events: WalletEvent[];
}

const assetToUrl = new Map<SupportedAssets, string>([
  [
    "ETH",
    "https://tokens.1inch.io/0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee.png",
  ],
  [
    "DAI",
    "https://tokens.1inch.io/0x6b175474e89094c44da98b954eedeac495271d0f.png",
  ],
  [
    "UNI",
    "https://tokens.1inch.io/0x1f9840a85d5af5bf1d1762f925bdaddc4201f984.png",
  ],
  [
    "AAVE",
    "https://tokens.1inch.io/0x7fc66500c84a76ad7e9c93437bfc5ac33e2ddae9.png",
  ],
  [
    "COMP",
    "https://tokens.1inch.io/0xc00e94cb662c3520282e6f5717214004a7f26888.png",
  ],
  [
    "SUSHI",
    "https://tokens.1inch.io/0x6b3595068778dd592e39a122f4f5a5cf09c90fe2.png",
  ],
  [
    "MKR",
    "https://tokens.1inch.io/0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2.png",
  ],
  [
    "YFI",
    "https://tokens.1inch.io/0x0bc529c00c6401aef6d220be8c6ea1667f6ad93e.png",
  ],
  [
    "GRT",
    "https://tokens.1inch.io/0xc944e90c64b2c07662a292be6244bdf05cda44a7.png",
  ],
  [
    "GTC",
    "https://tokens.1inch.io/0xde30da39c46104798bb5aa3fe8b9e0e1f348163f.png",
  ],
]);

export type {
  SupportedAssets,
  UserEvents,
  Balances,
  Events,
  WalletBalance,
  WalletEvent,
  AssetBalance,
  AssetInfo,
  AddressBalance,
  AddressEvents,
};

export { assetToUrl };
