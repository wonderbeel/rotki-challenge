import { createApp } from "vue";
import { VueQueryPlugin } from "@tanstack/vue-query";
import { autoAnimatePlugin } from "@formkit/auto-animate/vue";
import "virtual:windi.css";
import App from "./App.vue";

const app = createApp(App);
app.use(VueQueryPlugin);
app.use(autoAnimatePlugin);

app.mount("#app");
