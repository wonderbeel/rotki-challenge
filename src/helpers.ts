import { unref } from "vue";
import type { ComputedRef, Ref } from "vue";

const isValidAddress = (address: string) =>
  /0x(:?[0-9a-fA-F]{40})$/.test(address);

function deepUnref<T>(item: Ref<T> | ComputedRef<T> | T) {
  return unref(unref(item));
}

const calcPercentage = (value: number, total: number) => (value / total) * 100;

export { isValidAddress, deepUnref, calcPercentage };
