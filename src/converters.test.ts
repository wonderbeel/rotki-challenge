import { describe, expect, test } from "vitest";
import { ref } from "vue";
import {
  queryToAddressBalanceArray,
  queryToAddressEventsArray,
} from "./converters";

import type { WalletEvent } from "./types";

describe("queryToAddressBalanceArray", () => {
  test("convert undefined to an empty array", () => {
    const result = queryToAddressBalanceArray(ref(undefined));
    expect(result).toEqual([]);
  });

  test("filters away bad addresses", () => {
    const data = ref({
      invalid: {
        ETH: {
          amount: "10",
          usd_value: "500",
        },
      },
    });
    const result = queryToAddressBalanceArray(data);
    expect(result).toEqual([]);
  });

  test("convert query result", () => {
    const data = ref({
      "0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a46": {
        ETH: {
          amount: "10",
          usd_value: "500",
        },
      },
      "0xABC59231bfC2B3d308EB851541D4591CeA941FF3": {
        DAI: {
          amount: "5",
          usd_value: "500",
        },
        GRT: {
          amount: "20",
          usd_value: "20",
        },
      },
    });
    const result = queryToAddressBalanceArray(data);
    expect(result).toEqual([
      {
        address: "0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a46",
        balance: {
          ETH: {
            amount: "10",
            usd_value: "500",
          },
        },
      },
      {
        address: "0xABC59231bfC2B3d308EB851541D4591CeA941FF3",
        balance: {
          DAI: {
            amount: "5",
            usd_value: "500",
          },
          GRT: {
            amount: "20",
            usd_value: "20",
          },
        },
      },
    ]);
  });
});

describe("queryToAddressEventsArray", () => {
  test("convert undefined to an empty array", () => {
    const result = queryToAddressEventsArray(ref(undefined));
    expect(result).toEqual([]);
  });

  test("filters away bad addresses", () => {
    const events: WalletEvent[] = [
      {
        event_type: "deposit",
        asset: "DAI",
        value: {
          amount: "45",
          usd_value: "10",
        },
        timestamp: 100,
      },
    ];
    const data = ref({
      invalid: {
        events,
      },
    });
    const result = queryToAddressEventsArray(data);
    expect(result).toEqual([]);
  });

  test("convert query result", () => {
    const eventsA: WalletEvent[] = [
      {
        event_type: "deposit",
        asset: "ETH",
        value: {
          amount: "5",
          usd_value: "10",
        },
        timestamp: 100,
      },
    ];
    const eventsB: WalletEvent[] = [
      {
        event_type: "withdraw",
        asset: "ETH",
        value: {
          amount: "5",
          usd_value: "10",
        },
        timestamp: 100,
      },
      {
        event_type: "deposit",
        asset: "SUSHI",
        value: {
          amount: "500",
          usd_value: "15",
        },
        timestamp: 200,
      },
    ];
    const data = ref({
      "0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a46": {
        events: eventsA,
      },
      "0xABC59231bfC2B3d308EB851541D4591CeA941FF3": {
        events: eventsB,
      },
    });
    const result = queryToAddressEventsArray(data);
    expect(result).toEqual([
      {
        address: "0xABC11a5aCc3ad66025C21f24a91dD71D0Fc28a46",
        events: [
          {
            event_type: "deposit",
            asset: "ETH",
            value: {
              amount: "5",
              usd_value: "10",
            },
            timestamp: 100,
          },
        ],
      },
      {
        address: "0xABC59231bfC2B3d308EB851541D4591CeA941FF3",
        events: [
          {
            event_type: "withdraw",
            asset: "ETH",
            value: {
              amount: "5",
              usd_value: "10",
            },
            timestamp: 100,
          },
          {
            event_type: "deposit",
            asset: "SUSHI",
            value: {
              amount: "500",
              usd_value: "15",
            },
            timestamp: 200,
          },
        ],
      },
    ]);
  });
});
