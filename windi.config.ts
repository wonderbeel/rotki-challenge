import { defineConfig } from 'windicss/helpers'
import forms from "windicss/plugin/forms";
import heropatterns from "@windicss/plugin-heropatterns"

export default defineConfig({
  darkMode: 'media',
  plugins: [
    forms,
    heropatterns({
      patterns: ["tic-tac-toe"],
    })
  ]
})
