# Rotki Frontend Challenge

## Stack description
For this challenge I tried to keep the dependencies to a minimum, this is a list of packages used (and why)

- `vue`, `vite`, `typescript` and `json-server`: the required packages for the challenge
- `prettier` and `eslint`: consistent code style + linting
- `vitest` and `jsodom`: vitest has been used to run unit tests, because I am also testing helpers that makes use of DOM API (eg, `navigator`) jsdom is also required
- `cypress`: used to do component testing, e2e has not been configured because of the simplicity of the page
- `tanstack-query`: used to fetch data from the API managing stale while reloading cache, loading states, errors and converting the result to a reactive object
- `date-fns`: used to format timestamps into human readable dates
- `windicss` and `auto=animate`: used to style the UI and add some simple animations to the lists

## How to run
Dependencies are handled with `pnpm` and I tested everything under node 18 (lts/hydrogen)

### Run the app in dev mode
```
pnpm json-server
# in another terminal
pnpm dev
```

### Run the app in production mode
```
pnpm json-server
# in another terminal
pnpm build
pnpm serve
```

### Run unit tests
```
pnpm test-unit
```

### Run component tests
```
pnpm test-component
```

### Run cypress in interactive mode
```
pnpm cypress
```
